/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package graduate;

import static org.junit.Assert.*;

import org.junit.Test;

public class AppTest {
    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }

    @Test
    public void testAppHasAVersion() {
        App classUnderTest = new App();
        assertNotNull("app should return any version", classUnderTest.version());
    }
}
